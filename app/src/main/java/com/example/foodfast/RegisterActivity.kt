package com.example.foodfast

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private var controller: RegisterActivityController = RegisterActivityController()

    lateinit var continueButton : Button
    lateinit var userName : TextInputLayout
    lateinit var userLastName : TextInputLayout
    lateinit var userPhone : TextInputLayout
    lateinit var userPassword : TextInputLayout
    lateinit var userEmail : TextInputLayout
    lateinit var registerButton : Button
    lateinit var cancelButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        controller.registroResponse()
            .observe(this, Observer { response ->
                response?.let {
                    processRegisterResponse(response)
                }
            })

        continueButton = findViewById(R.id.register_button)
        userName = findViewById(R.id.name_edittext)
        userLastName = findViewById(R.id.lastname_edittext)
        userPhone = findViewById(R.id.phone_edittext)
        userEmail = findViewById(R.id.email_editText)
        userPassword = findViewById(R.id.password_editText)
        registerButton = findViewById(R.id.register_button)
        cancelButton = findViewById(R.id.cancel_button)


        registerButton.setOnClickListener {
            controller.registrarse(userEmail.editText?.text.toString(), userPassword.editText?.text.toString(), userName.editText?.text.toString(), userPhone.editText?.text.toString())
        }

        cancelButton.setOnClickListener {
            val intent: Intent = Intent(this,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }

    private fun processRegisterResponse(response: RegisterActivityController.RegistroResponse){
        when(response){
            is RegisterActivityController.RegistroResponse.BlankName -> onBlankName()
            is RegisterActivityController.RegistroResponse.BlankPhone -> onBlankPhone()
            is RegisterActivityController.RegistroResponse.BlankEmail -> onBlankEmail()
            is RegisterActivityController.RegistroResponse.BlankPassword -> onBlankPassword()
            is RegisterActivityController.RegistroResponse.WrongEmailFormat -> onWrongEmailFormat()
            is RegisterActivityController.RegistroResponse.InvalidPhoneLength -> onInvalidPhoneLength()
            is RegisterActivityController.RegistroResponse.OnlyDigits -> onOnlyDigits()
            is RegisterActivityController.RegistroResponse.AtLeastOneDigit -> onAtLeastOneDigit()
            is RegisterActivityController.RegistroResponse.AtLeastEightCharacters -> onAtLeastEightCharacters()
            is RegisterActivityController.RegistroResponse.RegisteredEmail -> onRegisteredEmail()
            is RegisterActivityController.RegistroResponse.CheckSuccess -> onCheckSuccess()
        }
    }

    private  fun performRegister(){
        var email = email_editText.editText?.text.toString()
        var password = password_editText.editText?.text.toString()

        //Firebase authentication
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful){
                    //Succesfull
                    //Log.d("Main", "Se creó el usuario: ${it.result?.user?.uid}")
                    saveUsertoFireBase()
                } else {
                    userEmail.editText?.error = getString(R.string.email_already_taken)
                }
            }
            .addOnFailureListener{
                //Log.d("Main", "Falló en crear usuario:${it.message}")
            }
    }

    private fun saveUsertoFireBase(){
        val uid = FirebaseAuth.getInstance().uid ?:""
        val ref = FirebaseFirestore.getInstance().document("/users/$uid")

        val user  = User(uid, name_edittext.editText?.text.toString(), lastname_edittext.editText?.text.toString(), phone_edittext.editText?.text.toString(),
            email_editText.editText?.text.toString())
        ref.set(user).addOnSuccessListener {
            Log.d("Register", "User in firebase")
            val intent: Intent = Intent(this,MenuActivity::class.java)
            startActivity(intent)
        }
    }

    private fun onBlankName(){
        userName.editText?.error = getString(R.string.mandatory_field)
    }
    private fun onBlankPhone(){
        userPhone.editText?.error = getString(R.string.mandatory_field)
    }
    private fun onBlankEmail(){
        userEmail.editText?.error = getString(R.string.mandatory_field)
    }
    private fun onBlankPassword(){
        userPassword.editText?.error = getString(R.string.mandatory_field)
    }
    private fun onRegisteredEmail(){
        userEmail.editText?.error = getString(R.string.email_already_taken)
    }
    private fun onWrongEmailFormat(){
        userEmail.editText?.error = getString(R.string.bad_email_format)
    }
    private fun onInvalidPhoneLength(){
        userPhone.editText?.error = getString(R.string.bad_phone_lenght)
    }
    private fun onOnlyDigits(){
        userPhone.editText?.error = getString(R.string.bad_phone_format)
    }
    private fun onAtLeastOneDigit(){
        userPassword.editText?.error = getString(R.string.at_least_one_digit_pass)
    }
    private fun onAtLeastEightCharacters(){
        userPassword.editText?.error = getString(R.string.at_least_eight_characters)
    }
    private fun onCheckSuccess(){
        performRegister()
    }

}

class User(val uid:String, val nombre: String, val apellido:String, val telefono : String, val email:String)

