package com.example.foodfast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class LoginActivityController {

    sealed class  LoginResponse{
        object BlankEmail : LoginResponse()
        object BlankPassword : LoginResponse()
        object WrongEmailFormat : LoginResponse()
        object CheckSuccess : LoginResponse()
    }

    private var loginResponse: MutableLiveData<LoginResponse> = MutableLiveData()

    fun loginResponse(): LiveData<LoginResponse> {
        return loginResponse
    }

    fun signIn(email: String, password: String){
        if(email.isBlank()){
            loginResponse.value = LoginResponse.BlankEmail
            return
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginResponse.value = LoginResponse.WrongEmailFormat
            return
        }
        if (password.isBlank()) {
            loginResponse.value = LoginResponse.BlankPassword
            return
        }
        loginResponse.value = LoginResponse.CheckSuccess

    }

}