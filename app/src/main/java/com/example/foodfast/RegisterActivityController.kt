package com.example.foodfast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class RegisterActivityController {

    sealed class RegistroResponse {
        object BlankName : RegistroResponse()
        object BlankPhone : RegistroResponse()
        object BlankEmail : RegistroResponse()
        object BlankPassword : RegistroResponse()
        object WrongEmailFormat : RegistroResponse()
        object InvalidPhoneLength : RegistroResponse()
        object OnlyDigits : RegistroResponse()
        object AtLeastOneDigit : RegistroResponse()
        object AtLeastEightCharacters : RegistroResponse()
        object RegisteredEmail : RegistroResponse()
        object CheckSuccess: RegistroResponse()

    }

    private var registroResponse: MutableLiveData<RegistroResponse> = MutableLiveData()

    fun registroResponse(): LiveData<RegistroResponse> {
        return registroResponse
    }
    fun registrarse(email: String, password: String, name: String, phoneNumber: String){

        val atLeastOneDigit = ".*[0-9].*".toRegex()
        if(name.isBlank()){
            registroResponse.value = RegistroResponse.BlankName
            return
        }
        if(phoneNumber.isBlank()){
            registroResponse.value = RegistroResponse.BlankPhone
            return
        }
        if(phoneNumber.length != 10){
            registroResponse.value = RegistroResponse.InvalidPhoneLength
            return
        }
        if(!phoneNumber.matches("[0-9]+".toRegex())){
            registroResponse.value = RegistroResponse.OnlyDigits
            return
        }
        if(email.isBlank()){
            registroResponse.value = RegistroResponse.BlankEmail
            return
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            registroResponse.value = RegistroResponse.WrongEmailFormat
            return
        }
        if(password.isBlank()){
            registroResponse.value = RegistroResponse.BlankPassword
            return
        }
        if(password.length < 8) {
            registroResponse.value = RegistroResponse.AtLeastEightCharacters
            return
        }
        if(!atLeastOneDigit.matches(password)) {
            registroResponse.value = RegistroResponse.AtLeastOneDigit
            return
        }

        registroResponse.value = RegistroResponse.CheckSuccess


    }
}