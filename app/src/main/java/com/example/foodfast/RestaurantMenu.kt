package com.example.foodfast

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_restaurant_menu.*
import kotlinx.android.synthetic.main.fragment_home.*


class RestaurantMenu : AppCompatActivity() {
  val db = FirebaseFirestore.getInstance()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_restaurant_menu)

       // val recibir = intent.getSerializableExtra("miLista") as? Array<String>

        //val recibir2 = intent.getSerializableExtra("miLista2") as? Array<String>

        val docRef = db.collection("restaurante").document("restaurante1")

        val docRef2 = db.collection("restaurante").document("restaurante2")

        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {

                    var datos = datosRestaurante(

                        nombre = document.getString("nombre"),

                        foto = document.getString("foto"),
                        direccion = document.getString("direccion"),
                        precioMenu1 = document.getString("precioMenu1")
                    )
                    menuImg.setOnClickListener {
                        startActivity(
                            Intent(this@RestaurantMenu,Pedido::class.java).putExtra("total",datos)
                        )
                    }
                }
            }






        primerRestaurante()

        menuImg.setOnClickListener(){

        }
       // segundoRestaurante()

    }




    fun primerRestaurante(){
        val recibir = intent.getSerializableExtra("miLista") as? Array<String>
        restaurantName?.text = recibir?.get(1)
        range?.text = recibir?.get(2)
        var imagen = recibir?.get(3)
        Glide.with(this).load(imagen).into(imgRestaurant)
        score?.text = getString(R.string.estrellas).plus(recibir?.get(5))

        menuTitle?.text = recibir?.get(6)
        menuDescription?.text = recibir?.get(7)
        menuTime?.text = getString(R.string.duracion).plus(recibir?.get(8))
        menuPrice?.text = getString(R.string.precio).plus(recibir?.get(9))
        var foto = recibir?.get(10)
        Glide.with(this).load(foto).centerCrop().into(menuImg)


    }

    fun segundoRestaurante(){
        val recibir2 = intent.getSerializableExtra("miLista2") as? Array<String>

        restaurantName?.text = recibir2?.get(1)
        range?.text = recibir2?.get(2)
        var imagen = recibir2?.get(3)
        Glide.with(this).load(imagen).into(menuImg)

        score?.text = recibir2?.get(5)

        menuTitle?.text = recibir2?.get(6)
        menuDescription?.text = recibir2?.get(7)
        menuTime?.text = recibir2?.get(8)
        menuPrice?.text = recibir2?.get(9)
        var foto = recibir2?.get(10)
        Glide.with(this).load(foto).into(menuImg)


    }



    private fun datosRestaurante(

        nombre: String?,
        foto: String?,
        direccion: String?,
        precioMenu1: String?
    ): Array<String?> {
        var inforRestaurante = arrayOf(
            nombre,
            foto,
            direccion,
            precioMenu1
        )
        return inforRestaurante
    }



}

