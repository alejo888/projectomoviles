package com.example.foodfast


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_home.*
import com.bumptech.glide.annotation.GlideModule
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
@GlideModule
class HomeFragment : Fragment() {

    val db = FirebaseFirestore.getInstance()
    //lateinit var nombreRestaurante: TextureView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment


        val docRef = db.collection("restaurante").document("restaurante1")

        val docRef2 = db.collection("restaurante").document("restaurante2")


        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    restName1.text = document.getString("nombre")
                    Glide.with(this)
                        .load("https://firebasestorage.googleapis.com/v0/b/foodfast-12dba.appspot.com/o/alitasdelcadillacb.png?alt=media&token=0769cb1e-6a29-45bc-bd10-4c5efbe3502c")
                        .into(imgRestaurant1)
                    Glide.with(this).load(
                        "https://firebasestorage.googleapis.com/v0/b/foodfast-12dba.appspot.com/o/hornerop.jpg?alt=media&token=6c4b8c1e-a47b-4354-ac97-03a2b4535156")
                        .into(imgRestaurant2)
                    var datos = datosRestaurante(
                        id = 1,
                        nombre = document.getString("nombre"),
                        rango = document.getString("rango"),
                        foto = document.getString("foto"),
                        direccion = document.getString("direccion"),
                        calificacion = document.getString("calificacion"),
                        nombreMenu1 = document.getString("nombreMenu1"),
                        descripcionMenu1 = document.getString("descripcionMenu1"),
                        tiempoMenu1 = document.getString("tiempoMenu1"),
                        precioMenu1 = document.getString("precioMenu1"),
                        fotoMenu1 = document.getString("fotoMenu1")
                    )
                    imgRestaurant1.setOnClickListener {
                        startActivity(
                            Intent(this@HomeFragment.context, RestaurantMenu::class.java)
                                .putExtra("miLista", datos)
                        )
                    }
                }
            }


        docRef2.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("exist", "DocumentSnapshot data: ${document.data}")
                    restName2.text = document.getString("nombre")

                    Glide.with(this).load(
                        "https://firebasestorage.googleapis.com/v0/b/foodfast-12dba.appspot.com/o/alitasdelcadillacb.png?alt=media&token=0769cb1e-6a29-45bc-bd10-4c5efbe3502c")
                        .into(imgRestaurant1)
                    var datos2 = datosRestaurante(
                        id = 1,
                        nombre = document.getString("nombre"),
                        rango = document.getString("rango"),
                        foto = document.getString("foto"),
                        direccion = document.getString("direccion"),
                        calificacion = document.getString("calificacion"),
                        nombreMenu1 = document.getString("nombreMenu1"),
                        descripcionMenu1 = document.getString("descripcionMenu1"),
                        tiempoMenu1 = document.getString("tiempoMenu1"),
                        precioMenu1 = document.getString("precioMenu1"),
                        fotoMenu1 = document.getString("fotoMenu1")
                    )

                    imgRestaurant2.setOnClickListener() {
                        startActivity(
                            Intent(this@HomeFragment.context, RestaurantMenu::class.java)
                                .putExtra("miLista2", datos2)
                        )
                    }



                }
            }

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    //var arreglo :Array<ejemplo> = arrayOf()
    //var selectedRestaurant : ejemplo? = null


    private fun datosRestaurante(
        id: Int,
        nombre: String?,
        rango: String?,
        foto: String?,
        direccion: String?,
        calificacion: String?,
        nombreMenu1: String?,
        descripcionMenu1: String?,
        fotoMenu1: String?,
        tiempoMenu1: String?,
        precioMenu1: String?
    ): Array<String?> {
        var inforRestaurante = arrayOf(
            id.toString(),
            nombre,
            rango,
            foto,
            direccion,
            calificacion,
            nombreMenu1,
            descripcionMenu1,
            tiempoMenu1,
            precioMenu1,
            fotoMenu1
        )
        return inforRestaurante
    }


    private fun passInfoRestaurant1() {


    }

    companion object {
        const val INTENT_ID_RESTAURANTE = "idRestaurant"
        const val INTENT_FOTO_RESTAURANTE = "imgRestaurante2"
        const val INTENT_ID_RESTAURANTE1 = "idRestaurant1"
        const val INTENT_FOTO_ALITAS = "imgRestaurante1"
    }


}



