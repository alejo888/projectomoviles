package com.example.foodfast

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var controller: LoginActivityController = LoginActivityController()
    lateinit var emailText: TextInputEditText
    lateinit var passwordText: TextInputEditText
    lateinit var loginButton: Button
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        controller.loginResponse()
            .observe(this, Observer { response ->
                response?.let {
                    processLoginResponse(response)
                }
            })

        emailText = findViewById(R.id.emailLoginText)
        passwordText = findViewById(R.id.passwordLoginText)
        loginButton= findViewById(R.id.btnLogin)
        auth = FirebaseAuth.getInstance()

        back_home.setOnClickListener {
            val intent: Intent = Intent(this,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }

    private fun processLoginResponse(response: LoginActivityController.LoginResponse){
        when(response){
            is LoginActivityController.LoginResponse.BlankEmail -> onBlankEmail()
            is LoginActivityController.LoginResponse.BlankPassword -> onBlankPassword()
            is LoginActivityController.LoginResponse.WrongEmailFormat -> onWrongEmailFormat()
            is LoginActivityController.LoginResponse.CheckSuccess -> onCheckSuccess()

        }
    }

    fun login(view: View){
        controller.signIn(emailText.text.toString(), passwordText.text.toString())
    }

    private  fun performLogin(){

        var email = emailLoginText.text.toString()
        var password = passwordLoginText.text.toString()

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful){
                    action()
                } else {
                    Toast.makeText(this, "Error en la autenticacion", Toast.LENGTH_LONG).show()
                }
            }

    }

    private fun action() {
        startActivity(Intent(this,MenuActivity::class.java))
    }

    private fun onBlankEmail(){
        emailText.error = getString(R.string.mandatory_field)
    }
    private fun onBlankPassword(){
        passwordText.error = getString(R.string.mandatory_field)
    }
    private fun onWrongEmailFormat(){
        emailText.error = getString(R.string.bad_email_format)
    }
    private fun onCheckSuccess(){
        performLogin()
    }

}