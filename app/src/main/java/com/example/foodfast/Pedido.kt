package com.example.foodfast

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_pedido.*

class Pedido : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pedido)

        var total = intent.getSerializableExtra("total") as? Array<String>

        pedido()

        pedidobutton.setOnClickListener() {
            Toast.makeText(this, "Pedido realizado", Toast.LENGTH_LONG).show()
            val intent: Intent = Intent(this,MenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
}


    fun pedido(){
        var total = intent.getSerializableExtra("total") as? Array<String>

        nameRestaurant?.text = total?.get(0)
        address?.text = total?.get(2)
        var imagen = total?.get(1)
        Glide.with(this).load(imagen).into(restaurantPhoto)
        totalTextView?.text = total?.get(3)
    }

}
